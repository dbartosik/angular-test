import { Student } from "./student";
import { Injectable } from "@angular/core";
import { Grade } from "./grade";

@Injectable()
export class StudentService {
    getStudents(): Promise<Student[]>{
        return Promise.resolve(
            [ new Student(1, "Jarek", "Bielecki"), 
            new Student(2, "Marcin", "Olszak"),
            new Student(3, "Marcin", "Miszczyk"),
            new Student(4, "Rafal", "Podlipny"),
            new Student(5, "Adam", "Swirski"),
            new Student(6, "Jedrzej", "Paszkowski"),
            new Student(7, "Jakub", "Sledzik"),
          ])
    }

    getStudentsGrades(): Promise<Grade[]>{
      var result = []

      for (var i = 1; i < 8; i++) {
        let counter = this.generateRand(10)
        for (var j = 0; j < counter; j++) {
          result.push(new Grade(i, this.generateRand(5)))
        }
      }
      return Promise.resolve(result)
    }
    
    generateRand(rand: number): number {
      return Math.ceil(Math.random()*rand)
    }
}  