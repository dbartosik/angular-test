import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListComponent } from './list/list.component';
import { StudentService } from './studentService';
import { StudentInfoProvider } from './studentInfoProvider';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [StudentService, StudentInfoProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
