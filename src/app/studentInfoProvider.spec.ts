import { StudentInfoProvider } from './studentInfoProvider';
import { Student } from './student';
import { StudentService } from './studentService';
import { tick, fakeAsync, async, TestBed } from '@angular/core/testing';
import { Grade } from './grade';

describe('StudentInfoProviderTest', () => {
  it('calculateAvgGrade_whenThereIsNoGrades_returnZero', () => {
    // Arrange
    let provider = new StudentInfoProvider(null)
    let grades = []

    // Act && Assert
    expect((<any>provider).calculateAvgGrade(grades)).toBeUndefined();
  });

  it('calculateAvgGrade_whenThereIsNoGrade_returnAvgRate', () => {
    // Arrange
    let provider = new StudentInfoProvider(null)
    let grades = [4,4,5,5]

    // Act && Assert
    expect((<any>provider).calculateAvgGrade(grades)).toBe(4.5);
  });

//---------------------------------------------------------------------------------------------

let stServiceSpy = jasmine.createSpyObj('StudentService', ['getStudents', 'getStudentsGrades']);

beforeEach(async(() => {
  TestBed.configureTestingModule({
    providers: [
      StudentInfoProvider,
      { provide: StudentService, useValue: stServiceSpy }
    ]
  });
}))

  it('getStudentInfoList_mapsThreeStudentsToThreeStudentsInfoObjects', fakeAsync(() => {
    // Arrange
    stServiceSpy.getStudents.and.returnValue(Promise.resolve([
      new Student(1, "a", "b"),
      new Student(1, "a", "b"),
      new Student(1, "a", "b")
    ]))
    stServiceSpy.getStudentsGrades.and.returnValue(Promise.resolve([]))

    let provider = TestBed.get(StudentInfoProvider)
    let result = [];

    // Act
    provider.getStudentInfoList().then(r => result = r)
    tick()

    // Assert
    expect(result.length).toBe(3);
  }));


  it('getStudentInfoList_studentInfoShouldContainAvgGrade', fakeAsync(() => {
    // Arrange

    stServiceSpy.getStudents.and.returnValue(Promise.resolve([
      new Student(1, "a", "b")
    ]))
    stServiceSpy.getStudentsGrades.and.returnValue(Promise.resolve([
      new Grade(1, 2),
      new Grade(1, 5)
    ]))

    let provider = TestBed.get(StudentInfoProvider)
    let result = [];

    // Act
    provider.getStudentInfoList().then(r => result = r)
    tick()

    // Assert
    expect(result[0].avgGrade).toBeTruthy();
  }));
});

