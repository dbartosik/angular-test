import { StudentService } from "./studentService";
import { StudentInfo } from "./studentInfo";
import { Injectable } from "@angular/core";

@Injectable()
export class StudentInfoProvider
{
    constructor(private listService : StudentService){
    }

    getStudentInfoList() : Promise<StudentInfo[]>{
        return new Promise<StudentInfo[]>((resolve, reject)=>{
            this.listService.getStudents().then(students => {
                this.listService.getStudentsGrades().then(grades =>{
                    let result = students.map(s => {
                        return new StudentInfo(s.number, s.name, s.lastName, 
                            this.calculateAvgGrade(grades.filter(g => g.studentNumber == s.number)
                                                        .map(gr => { return gr.value })))
                    })
                    resolve(result)
               })
            })
        })
    }

    private calculateAvgGrade(grades: number[]): number {
        let sum = 0
        grades.forEach(g => sum+= g)
        return grades.length == 0 ? undefined : sum/grades.length
    }
}