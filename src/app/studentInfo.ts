export class StudentInfo{
  constructor(public number: Number, public name: string, public lastName: string, public avgGrade: Number) {

  }
}