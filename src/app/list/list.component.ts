import { Component, OnInit } from '@angular/core';
import { StudentInfoProvider } from '../studentInfoProvider';
import { StudentInfo } from '../studentInfo';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  public students: StudentInfo[] = []
  constructor(private studentInfoProvider: StudentInfoProvider) { }

  ngOnInit() {
    this.studentInfoProvider.getStudentInfoList().then( r => {
      this.students = r
    })
  }
}
