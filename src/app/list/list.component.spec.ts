import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { ListComponent } from './list.component';
import { StudentInfoProvider } from '../studentInfoProvider';
import { StudentInfo } from '../studentInfo';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let studentInfoProvider: StudentInfoProvider

  beforeEach(async(() => {
    studentInfoProvider = {
      getStudentInfoList: () => {
        return Promise.resolve([])
      }
    } as StudentInfoProvider;

    TestBed.configureTestingModule({
      declarations: [ListComponent],
      providers: [
        { provide: StudentInfoProvider, useValue: studentInfoProvider }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit_invokeStudentInfoProvider', fakeAsync(() => {
    //Arrange
    let invoked = false
    let stProv = TestBed.get(StudentInfoProvider)
    stProv.getStudentInfoList = () => {
      invoked = true
      return Promise.resolve([])
    }

    // Act 
    component.ngOnInit()

    // Assert
    expect(invoked).toBeTruthy()
  }));

  it('ngOnInit_setDataTpStudents', fakeAsync(() => {
    // Arrange
    let stProv = TestBed.get(StudentInfoProvider)
    stProv.getStudentInfoList = () => {
      return Promise.resolve([new StudentInfo(1, "aaa", "bbb", 0)])
    }

    // Act
    component.ngOnInit()
    tick()

    // Assert
    expect(component.students.length == 1).toBeTruthy()
  }));
});